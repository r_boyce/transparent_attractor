class TransparentAttractorAnimation {

	constructor(window, document, parentDiv) {
		this.timelines = [];

		// add template html to the page
		const attractorTemplate = `<div id="attractor-circle" class="circle">
	  		<div class="circle circle-lg"></div>
	  		<div class="circle circle-sm"></div>

	  	</div>
	  	<div class="inner">
		  	<div id="attractor-hand" class="hand">
		  		<img src="assets/noun_303000_cc.svg">
		  	</div>
		  	<div id="attractor-text" class="text">
		  		Touch to Explore
		  	</div>
	  	</div>`
	  	parentDiv.innerHTML = attractorTemplate;
		const circleDiv = document.getElementById('attractor-circle');
		const handDiv = document.getElementById('attractor-hand');

		// Helper functions to convert to viewport units
		const vw = (coef) => window.innerWidth * (coef/100);
		const vh = (coef) => window.innerHeight * (coef/100);

		// Timeline options: play forward & backward, repeat forever
		const tlOpts = {
			yoyo: true,
			repeat: -1

		};

		// Background circle pulse animation
		const circleTl = new TimelineMax(tlOpts);
		circleTl.to(circleDiv, 0.75, {
			transform: 'scale(1.1)',

		});
		this.timelines.push(circleTl);

		// Attractor animation
		const attractorTl = new TimelineMax(tlOpts);
		attractorTl
			   .to(parentDiv, 3, {x: vw(10), y: vh(10)})
			   .to(parentDiv, 3, {x: vw(12), y: vh(55)})
			   .to(parentDiv, 3, {x: vw(55), y: vh(60)})
			   .to(handDiv, 0.5, {rotationZ: -30})
			   .to(handDiv, 0.5, {rotationZ: 0})
			   .to(parentDiv, 3, {x: vw(54), y: vh(12)})
			   .to(handDiv, 0.5, {rotationZ: -30})
			   .to(handDiv, 0.5, {rotationZ: 0})
			   .to(parentDiv, 3, {x: vw(10), y: vh(10)})
			   ;
		this.timelines.push(attractorTl);
	}

	start() {
		this.timelines.forEach(function(tl) {
			tl.play();
		});
	}

	restart() {
		this.timelines.forEach(function(tl) {
			tl.restart();
		});
	}

	stop() {
		this.timelines.forEach(function(tl) {
			tl.pause();
		});
	}

}