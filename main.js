window.onload = () => {
	var parentDiv = document.getElementsByClassName('attractor')[0];
	var animation = new TransparentAttractorAnimation(window, document, parentDiv);
	animation.start();
	window.setTimeout(function() {
		animation.stop();
	}, 1000);
	window.setTimeout(function() {
		animation.start();
	}, 2000);
	window.setTimeout(function() {
		animation.stop();
	}, 3000);
	window.setTimeout(function() {
		animation.restart();
	}, 4000);
};		

